package datastructures

import "fmt"

type SinglyLinkedList struct {
	head *Node
}

// Linked list methods

func (ll *SinglyLinkedList) InsertBeginning(value interface{}) {
	node := &Node{value: value}
	if ll.head == nil {
		ll.head = node
	} else {
		node.next = ll.head
		ll.head = node
	}
}

func (ll *SinglyLinkedList) InsertAfter(after, value interface{}) {
	for curr := ll.head; curr != nil; curr = curr.next {
		if curr.value == after {
			node := &Node{value: value, next: curr.next}
			curr.next = node
			return
		}
	}
}

func (ll *SinglyLinkedList) RemoveBeginning() {
	if ll.head != nil {
		ll.head = ll.head.next
	}
}

func (ll *SinglyLinkedList) RemoveAfter(after interface{}) {
	for curr := ll.head; curr != nil; curr = curr.next {
		if curr.value == after {
			if curr.next.next == nil {
				curr.next = nil
			} else {
				curr.next = curr.next.next
			}
			return
		}
	}
}

func (ll *SinglyLinkedList) Display() {
	for curr := ll.head; curr != nil; curr = curr.next {
		fmt.Printf("%+v\n", curr)
	}
}

func (ll *SinglyLinkedList) Search(value interface{}) interface{} {
	for curr := ll.head; curr != nil; curr = curr.next {
		if curr.value == value {
			return curr
		}
	}
	return nil
}

// List methods

func (ll *SinglyLinkedList) Insert(index int, value interface{}) {
	size := ll.Size()
	prev := new(Node)
	nodeVal := value.(*Node)
	for i, curr := 0, ll.head; i < size; i, curr = i+1, curr.next {
		if i == index {
			if prev == nil {
				curr = nodeVal
			} else {
				prev.next = nodeVal
				nodeVal.next = curr
			}
		}
		prev = curr
	}
}

func (ll *SinglyLinkedList) Get(index int) (interface{}, bool) {
	size := ll.Size()
	for i, curr := 0, ll.head; i < size; i, curr = i+1, curr.next {
		if i == index {
			return curr, true
		}
	}
	return nil, false
}

func (ll *SinglyLinkedList) Remove(index int) {
	size := ll.Size()
	prev := new(Node)
	for i, curr := 0, ll.head; i < size; i, curr = i+1, curr.next {
		if i == index {
			if prev.next == nil {
				ll.head = curr.next
			} else {
				prev.next = curr.next
			}
			break
		}
		prev = curr
	}
}

func (ll *SinglyLinkedList) Contains(value interface{}) bool {
	currentNode := ll.head
	for currentNode != nil {
		if currentNode.value == value {
			return true
		}
		currentNode = currentNode.next
	}
	return false
}

// Collection methods

func (ll *SinglyLinkedList) IsEmpty() bool {
	return ll.head == nil
}

func (ll *SinglyLinkedList) Clear() {
	ll.head = nil
}

func (ll *SinglyLinkedList) Size() (size int) {
	currentNode := ll.head
	for currentNode != nil {
		size++
		currentNode = currentNode.next
	}
	return size
}

func (ll *SinglyLinkedList) ToSlice() []interface{} {
	size := ll.Size()
	slice := make([]interface{}, size)
	currentNode := ll.head
	for i := 0; i < size; i++ {
		slice[i] = *currentNode
		currentNode = currentNode.next
	}
	return slice
}
