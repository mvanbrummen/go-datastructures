package datastructures

type Node struct {
	value interface{}
	next  *Node
}

func (n *Node) SetValue(value interface{}) {
	n.value = value
}

func (n *Node) Value() interface{} {
	return n.value
}
