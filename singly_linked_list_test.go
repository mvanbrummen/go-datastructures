package datastructures

import "testing"

func TestInsertBeginning(t *testing.T) {
	var tests = []struct {
		nodes    []string
		expected *Node
	}{
		{[]string{"test"}, &Node{value: "test"}},
		{[]string{"test", "boourns"}, &Node{value: "boourns"}},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test.nodes {
			sll.InsertBeginning(node)
		}
		if sll.head.value != test.expected.value {
			t.Error("For", test.nodes, "Expected", test.expected.value, "But got", sll.head.value)
		}
	}
}

func TestRemoveBeginning(t *testing.T) {
	var tests = []struct {
		nodes    []string
		expected *Node
	}{
		{[]string{"test"}, nil},
		{[]string{"test", "boourns"}, &Node{value: "test"}},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test.nodes {
			sll.InsertBeginning(node)
		}
		sll.RemoveBeginning()
		if test.expected == nil {
			if sll.head != nil {
				t.Error("For", test.nodes, "Expected", test.expected, "But got", sll.head)
			}
		} else {
			if sll.head.value != test.expected.value {
				t.Error("For", test.nodes, "Expected", test.expected, "But got", sll.head)
			}
		}
	}
}

func TestSearch(t *testing.T) {
	var tests = []struct {
		nodes    []int
		search   int
		expected *Node
	}{
		{[]int{1, 2, 3, 4, 5, 6}, 4, &Node{value: 4}},
		{[]int{1, 2, 3, 4, 5, 6}, 1, &Node{value: 1}},
		{[]int{1, 2, 3, 4, 5, 6}, 6, &Node{value: 6}},
		{[]int{1, 2, 3, 4, 5, 6}, 90, nil},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test.nodes {
			sll.InsertBeginning(node)
		}
		actual, _ := sll.Search(test.search).(*Node)
		if test.expected == nil {
			if actual != nil {
				t.Error("For", test.nodes, "Expected", test.expected.value, "But got", actual.value)
			}
		} else {
			if actual.value != test.expected.value {
				t.Error("For", test.nodes, "Expected", test.expected.value, "But got", actual.value)
			}
		}
	}
}

func TestContains(t *testing.T) {
	var tests = []struct {
		nodes    []int
		contains int
		expected bool
	}{
		{[]int{1, 2, 3, 4, 5, 6}, 4, true},
		{[]int{1, 2, 3, 4, 5, 6}, 1, true},
		{[]int{1, 2, 3, 4, 5, 6}, 6, true},
		{[]int{1, 2, 3, 4, 5, 6}, 90, false},
		{[]int{1, 2, 3, 4, 5, 6}, -90, false},
		{[]int{1, 2, 3, 4, 5, -6}, -6, true},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test.nodes {
			sll.InsertBeginning(node)
		}
		if cont := sll.Contains(test.contains); cont != test.expected {
			t.Error("For", test.nodes, "Expected", test.expected, "But got", cont)
		}
	}
}

func TestIsEmpty(t *testing.T) {
	var tests = []struct {
		nodes    []int
		expected bool
	}{
		{[]int{1, 2, 3, 4, 5}, false},
		{[]int{1}, false},
		{nil, true},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test.nodes {
			sll.InsertBeginning(node)
		}
		if empty := sll.IsEmpty(); empty != test.expected {
			t.Error("For", test.nodes, "Expected", test.expected, "But got", empty)
		}
	}
}

func TestClear(t *testing.T) {
	var tests = [][]int{
		{1, 2, 3, 4, 5},
		{1},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test {
			sll.InsertBeginning(node)
		}
		sll.Clear()
		if empty := sll.IsEmpty(); empty != true {
			t.Error("For", tests, "Expected", true, "But got", empty)
		}
	}
}

func TestSize(t *testing.T) {
	var tests = []struct {
		nodes    []int
		expected int
	}{
		{[]int{1, 2, 3, 4, 5}, 5},
		{nil, 0},
		{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9}, 9},
	}
	for _, test := range tests {
		sll := new(SinglyLinkedList)
		for _, node := range test.nodes {
			sll.InsertBeginning(node)
		}
		if size := sll.Size(); size != test.expected {
			t.Error("For", test.nodes, "Expected", test.expected, "But got", size)
		}
	}
}
