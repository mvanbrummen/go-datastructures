package datastructures

type (
	Collection interface {
		Size() int
		Clear()
		IsEmpty() bool
		ToSlice() []interface{}
	}

	List interface {
		Insert(index int, value interface{})
		Get(index int) (interface{}, bool)
		Remove(index int)
		Contains(value interface{}) bool

		Collection
	}

	LinkedList interface {
		Display()
		Search(value interface{}) interface{}
		InsertBeginning(value interface{})
		InsertAfter(after, value interface{})
		RemoveBeginning()
		RemoveAfter(after interface{})

		List
	}

	Stack interface {
		Push(interface{})
		Pop() interface{}
		Peek() interface{}
		IsFull() bool

		Collection
	}
)
